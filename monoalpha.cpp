#include <bits/stdc++.h>
using namespace std;

string key = "YQJRTWCUZKEOMBPLVDSXHAGNFIyqjrtwcuzkeombplvdsxhagnfi";
string alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
map <char, char> encMap;
map<char, char> decMap;

void mappingEncKey() {
    for(int i = 0; i < key.length(); i++) {
        encMap[alpha[i]] = key[i];
    }
}

void mappingDecKey() {
    for(int i = 0; i < key.length(); i++) {
        decMap[key[i]] = alpha[i];
    }
}

string encryption(string plainText) {
    mappingEncKey();
    string cipherText = "";
    for(int i = 0; i < plainText.length(); i++) {
        if(!isspace(plainText[i])) {
            cipherText += encMap[plainText[i]];
        } else {
            cipherText += " ";
        }
    }
    return cipherText;
}

string decryption(string cipherText) {
    mappingDecKey();
    string plainText = "";
    for(int i = 0; i < cipherText.length(); i++) {
        if(!isspace(cipherText[i])) {
            plainText += decMap[cipherText[i]];
        } else {
            plainText += " ";
        }
    }
    return plainText;
}
 
int main() {
    string plainText = "Check";
    string cipherText = encryption(plainText);
    cout<<cipherText<<endl;
    cout<<decryption(cipherText)<<endl;
}